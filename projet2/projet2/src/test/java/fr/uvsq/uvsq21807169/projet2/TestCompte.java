package fr.uvsq.uvsq21807169.projet2;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCompte {

	Compte projet2;
	
	
	@org.junit.Before
	public void testInit(){
		projet2 = new Compte(5);
	}
	/*
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	*/
	
	
	@org.junit.Test(expected =IllegalArgumentException.class)
	public void testInitExp() {
		Compte cp = new Compte(-2);
	}
	
	@org.junit.Test
	public void testGet() {
		assertEquals(projet2.getSold(), 5);
	}
	
	
	
	@org.junit.Test
	public void testCredit() {
		projet2.crediter(12);
		assertEquals(projet2.getSold(), 17);
	}
	
	@org.junit.Test
	public void testDebit() {
		projet2.debiter(2);
		assertEquals(projet2.getSold(), 3);
	}
	
	
	@org.junit.Test
	public void testVirement() {
		Compte cp = new Compte(10);
		projet2.virement(cp, 2);
		assertEquals(projet2.getSold(), 3);
		assertEquals(cp.getSold(), 12);

	}
	

}
	