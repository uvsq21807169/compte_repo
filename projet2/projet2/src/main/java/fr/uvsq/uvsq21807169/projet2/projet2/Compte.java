package fr.uvsq.uvsq21807169.projet2.projet2;

public class Compte {

	private int sold;
	
	public Compte(int s) {
		if(s < 0)
			throw new IllegalArgumentException();
		sold = s;
	}
	
	public int getSold() {
		return sold;
	}
	
	public void crediter(int s) {
		if(s < 0)
			throw new IllegalArgumentException();
		else {
			sold += s;
		}
	}
	
	public void debiter(int s) {
		if(s < 0)
			throw new IllegalArgumentException();
		else
			if(s > sold)
					throw new IllegalArgumentException();
			else
				sold -= s;
	}
	
	public void virement(Compte cp, int s) {
		this.debiter(s);
		cp.crediter(s);
	}


}
