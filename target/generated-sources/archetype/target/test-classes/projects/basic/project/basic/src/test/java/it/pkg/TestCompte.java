package it.pkg;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCompte {

	Compte basic;
	
	
	@org.junit.Before
	public void testInit(){
		basic = new Compte(5);
	}
	/*
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	*/
	
	
	@org.junit.Test(expected =IllegalArgumentException.class)
	public void testInitExp() {
		Compte cp = new Compte(-2);
	}
	
	@org.junit.Test
	public void testGet() {
		assertEquals(basic.getSold(), 5);
	}
	
	
	
	@org.junit.Test
	public void testCredit() {
		basic.crediter(12);
		assertEquals(basic.getSold(), 17);
	}
	
	@org.junit.Test
	public void testDebit() {
		basic.debiter(2);
		assertEquals(basic.getSold(), 3);
	}
	
	
	@org.junit.Test
	public void testVirement() {
		Compte cp = new Compte(10);
		basic.virement(cp, 2);
		assertEquals(basic.getSold(), 3);
		assertEquals(cp.getSold(), 12);

	}
	

}
	