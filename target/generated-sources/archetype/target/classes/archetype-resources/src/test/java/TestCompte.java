#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCompte {

	Compte ${artifactId};
	
	
	@org.junit.Before
	public void testInit(){
		${artifactId} = new Compte(5);
	}
	/*
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	*/
	
	
	@org.junit.Test(expected =IllegalArgumentException.class)
	public void testInitExp() {
		Compte cp = new Compte(-2);
	}
	
	@org.junit.Test
	public void testGet() {
		assertEquals(${artifactId}.getSold(), 5);
	}
	
	
	
	@org.junit.Test
	public void testCredit() {
		${artifactId}.crediter(12);
		assertEquals(${artifactId}.getSold(), 17);
	}
	
	@org.junit.Test
	public void testDebit() {
		${artifactId}.debiter(2);
		assertEquals(${artifactId}.getSold(), 3);
	}
	
	
	@org.junit.Test
	public void testVirement() {
		Compte cp = new Compte(10);
		${artifactId}.virement(cp, 2);
		assertEquals(${artifactId}.getSold(), 3);
		assertEquals(cp.getSold(), 12);

	}
	

}
	