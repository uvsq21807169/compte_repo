#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Compte ${artifactId} = new Compte(3);
        System.out.println("Juste pour le test ...");
    }
}
