package fr.uvsq.uvsq21807169.compte;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestCompte {

	Compte compte;
	
	
	@org.junit.Before
	public void testInit(){
		compte = new Compte(5);
	}
	/*
	@Test
	public void test() {
		fail("Not yet implemented");
	}
	*/
	
	
	@org.junit.Test(expected =IllegalArgumentException.class)
	public void testInitExp() {
		Compte cp = new Compte(-2);
	}
	
	@org.junit.Test
	public void testGet() {
		assertEquals(compte.getSold(), 5);
	}
	
	
	
	@org.junit.Test
	public void testCredit() {
		compte.crediter(12);
		assertEquals(compte.getSold(), 17);
	}
	
	@org.junit.Test
	public void testDebit() {
		compte.debiter(2);
		assertEquals(compte.getSold(), 3);
	}
	
	
	@org.junit.Test
	public void testVirement() {
		Compte cp = new Compte(10);
		compte.virement(cp, 2);
		assertEquals(compte.getSold(), 3);
		assertEquals(cp.getSold(), 12);

	}
	

}
	