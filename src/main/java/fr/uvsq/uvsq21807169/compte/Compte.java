package fr.uvsq.uvsq21807169.compte;

public class Compte {

	private int sold;
	
	/**
	 * constructeur
	 * @param s : solde a credité a l'ouverture du compte
	 */
	public Compte(int s) {
		if(s < 0)
			throw new IllegalArgumentException();
		sold = s;
	}
	
	/**
	 * retourner le solde actuel
	 * @return
	 */
	public int getSold() {
		return sold;
	}
	
	/**
	 * ajouter une somme au compte
	 * @param s
	 */
	public void crediter(int s) {
		if(s < 0)
			throw new IllegalArgumentException();
		else {
			sold += s;
		}
	}
	
	/**
	 * retirer une somme
	 * @param s
	 */
	public void debiter(int s) {
		if(s < 0)
			throw new IllegalArgumentException();
		else
			if(s > sold)
					throw new IllegalArgumentException();
			else
				sold -= s;
	}
	
	/**
	 * Faire un virrement
	 * @param cp
	 * @param s
	 */
	public void virement(Compte cp, int s) {
		this.debiter(s);
		cp.crediter(s);
	}


}
